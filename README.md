# Future Interview
My implementation of an appointment scheduling API. Database calls are mocked. 

# RUN INSTRUCTIONS
Run with any IDE.
It will run on http://localhost:7000/ 

You can then hit the endpoint of your choice. Examples:

To get all available appointments of trainer 5 between a custom date range:
http://localhost:7000/trainers/5/available?start_date=2:00&end_date=4:00

To get all scheduled appointments of trainer 4:
http://localhost:7000/trainers/4/scheduled

To schedule an appointment with trainer 3:
POST to http://localhost:7000/trainers/3/schedule (I used Postman for this)
Example body to POST:
{
    "user_id":"1",
    "start_date":"11:00",
    "end_date":"11:30"
}