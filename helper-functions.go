package main

import (
	"encoding/json"
	"errors"
	"net/http"
)

func validateDates(startDate, endDate string) error {
	//mocked here, but this where the logic for ensuring the date format is correct e.g. scheduled at :00, :30 during business hours
	if true {
		return nil
	} else {
		return errors.New("incorrect date format: *EXPLANATION*")
	}
}

func handleResponseError(w http.ResponseWriter, code int, message string) {
	handleResponse(w, code, map[string]string{"error": message})
}

func handleResponse(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
