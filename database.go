package main

func getAvailableAppointmentsBetweenDateRange(startDate, endDate, trainerID string) ([]Appointment, error) {
	//mocked here, but this is where a SQL query would be written to retrieve the trainer's available appointments in this range
	return []Appointment{
		{
			TrainerID: trainerID,
			StartDate: "5/6/2021 @ 1:00PM",
			EndDate:   "5/6/2021 @ 1:30PM",
		},
		{
			TrainerID: trainerID,
			StartDate: "5/6/2021 @ 2:00PM",
			EndDate:   "5/6/2021 @ 2:30PM",
		},
		{
			TrainerID: trainerID,
			StartDate: "5/6/2021 @ 4:00PM",
			EndDate:   "5/6/2021 @ 4:30PM",
		},
		{
			TrainerID: trainerID,
			StartDate: "5/7/2021 @ 1:30PM",
			EndDate:   "5/7/2021 @ 2:00PM",
		},
		{
			TrainerID: trainerID,
			StartDate: "5/8/2021 @ 1:00PM",
			EndDate:   "5/8/2021 @ 1:30PM",
		},
	}, nil
}

func getScheduledAppointments(trainerID string) ([]Appointment, error) {
	//mocked here, but this is where a SQL query would be written to retrieve all of the trainer's scheduled appointments
	return []Appointment{
		{
			AppointmentID: "1",
			UserID:        "1",
			TrainerID:     trainerID,
			StartDate:     "5/6/2021 @ 1:00PM",
			EndDate:       "5/6/2021 @ 1:30PM",
		},
		{
			AppointmentID: "2",
			UserID:        "2",
			TrainerID:     trainerID,
			StartDate:     "5/6/2021 @ 2:00PM",
			EndDate:       "5/6/2021 @ 2:30PM",
		},
		{
			AppointmentID: "3",
			UserID:        "3",
			TrainerID:     trainerID,
			StartDate:     "5/6/2021 @ 4:00PM",
			EndDate:       "5/6/2021 @ 4:30PM",
		},
		{
			AppointmentID: "4",
			UserID:        "2",
			TrainerID:     trainerID,
			StartDate:     "5/7/2021 @ 1:30PM",
			EndDate:       "5/7/2021 @ 2:00PM",
		},
		{
			AppointmentID: "5",
			UserID:        "3",
			TrainerID:     trainerID,
			StartDate:     "5/8/2021 @ 1:00PM",
			EndDate:       "5/8/2021 @ 1:30PM",
		},
	}, nil
}

func insertNewAppointment(appointment Appointment) (Appointment, error) {
	//mocked here, but here is where we would write a query to insert the new appointment into the SQL db
	return Appointment{
		UserID:    appointment.UserID,
		TrainerID: appointment.TrainerID,
		StartDate: appointment.StartDate,
		EndDate:   appointment.EndDate,
	}, nil
}
