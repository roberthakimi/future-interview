package main

type Appointment struct {
	AppointmentID string `json:"appointment_id,omitempty"`
	TrainerID     string `json:"trainer_id"`
	UserID        string `json:"user_id,omitempty"`
	StartDate     string `json:"start_date"`
	EndDate       string `json:"end_date"`
}
