package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func futureHome(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Welcome to Future!")
}

//start date and end date will be given as query parameters
func availableAppointments(w http.ResponseWriter, r *http.Request) {
	vars := r.URL.Query()
	startDate := vars.Get("start_date")
	endDate := vars.Get("end_date")
	trainerID := mux.Vars(r)["id"]

	err := validateDates(startDate, endDate)
	if err != nil {
		handleResponseError(w, http.StatusBadRequest, "dates provided are not valid")
	}

	res, err := getAvailableAppointmentsBetweenDateRange(startDate, endDate, trainerID)
	if err != nil {
		handleResponseError(w, http.StatusBadRequest, fmt.Sprintf("error retrieving available appointmets for trainer %s: %v", trainerID, err))
	}

	handleResponse(w, http.StatusOK, res)
}

func scheduleNewAppointment(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	trainerID := mux.Vars(r)["id"]
	var appointment Appointment
	err := json.Unmarshal(reqBody, &appointment)
	if err != nil {
		handleResponseError(w, http.StatusBadRequest, fmt.Sprintf("failed to unmarshal post body into appointment: %v", err))
	}
	appointment.TrainerID = trainerID

	res, err := insertNewAppointment(appointment)
	if err != nil {
		handleResponseError(w, http.StatusBadRequest, fmt.Sprintf("error retrieving available appointmets for trainer %s: %v", appointment.TrainerID, err))
	}

	handleResponse(w, http.StatusOK, res)
}

func scheduledAppointments(w http.ResponseWriter, r *http.Request) {
	trainerID := mux.Vars(r)["id"]

	res, err := getScheduledAppointments(trainerID)
	if err != nil {
		handleResponseError(w, http.StatusBadRequest, fmt.Sprintf("error retrieving scheduled appointmets for trainer %s: %v", trainerID, err))
	}

	handleResponse(w, http.StatusOK, res)
}

func handleRequests() {
	router := mux.NewRouter()
	router.HandleFunc("/", futureHome)

	//I chose to organize the trainerID in the endpoint path as opposed to retrieving from query params or POST body
	//as the ID will be needed for every endpoint.
	router.HandleFunc("/trainers/{id}/available", availableAppointments).Methods(http.MethodGet)
	router.HandleFunc("/trainers/{id}/schedule", scheduleNewAppointment).Methods(http.MethodPost)
	router.HandleFunc("/trainers/{id}/scheduled", scheduledAppointments).Methods(http.MethodGet)

	log.Fatal(http.ListenAndServe(":7000", router))
}

func main() {
	fmt.Print("hello world")
	handleRequests()
}
